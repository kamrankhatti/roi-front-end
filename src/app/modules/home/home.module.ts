import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeRoutes } from './home.routes';
import { SharedModule } from '../shared/shared.module';

/*Components*/
import { HomeComponent } from './component/home.component';
import { AddPlaylistComponent } from './modals/add-playlist/add-playlist.component';

@NgModule({
  declarations: [
    HomeComponent,
    AddPlaylistComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(HomeRoutes)
  ],
  entryComponents: [
     AddPlaylistComponent
   ]
})
export class HomeModule { }
