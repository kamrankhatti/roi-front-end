import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-add-playlist',
    templateUrl: './add-playlist.component.html',
    styleUrls: ['./add-playlist.component.css']
})
export class AddPlaylistComponent {
    tag = '';
    likedName = [];
    likedArtistName = [];

    constructor(public modalRef: BsModalRef) {
    }

    ngOnInit() {
        //get artist name from lcoal storage
        this.likedArtistName = JSON.parse(localStorage.getItem('likedArtistName') || '[]');
        //get artist complete object from local storage
        this.likedName = JSON.parse(localStorage.getItem('likedName') || '[]');
    }

    isAdded(row) {
        return this.likedArtistName.indexOf(row.name) !== -1;
    }

    addPlaylist(row) {
        const isAdded = this.isAdded(row);

        if (isAdded) {
            this.likedName = this.likedName.filter(($item) => {
                return $item.name != row.name;
            });

            this.likedArtistName = this.likedArtistName.filter(($item) => {
                return $item != row.name;
            });

        } else {
            this.likedArtistName.push(row.name);
            this.likedName.push(row);
        }

        localStorage.setItem('likedArtistName', JSON.stringify(this.likedArtistName));
        localStorage.setItem('likedName', JSON.stringify(this.likedName));
    }

    onSumbitForm(tagForm) {
        this.modalRef.content.artist.tag = this.tag;
        this.addPlaylist(this.modalRef.content.artist);
        this.modalRef.hide();
    }


}
