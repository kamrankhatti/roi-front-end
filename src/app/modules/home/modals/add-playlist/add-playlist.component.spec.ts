import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule }   from '@angular/forms';
import { AddPlaylistComponent } from './add-playlist.component';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

describe('AddPlaylistComponent', () => {
    let component:AddPlaylistComponent;
    let fixture:ComponentFixture<AddPlaylistComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddPlaylistComponent],
            imports: [RouterTestingModule, FormsModule],
            providers: [BsModalRef]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddPlaylistComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should return false if Playlist is not added.', () => {
        expect(component.isAdded({name: 'foo'})).toBeFalsy();
    });

    it('should return true if Playlist is already added.', () => {
        component.likedArtistName = ['foo'];
        expect(component.isAdded({name: 'foo'})).toBeTruthy();
    });

});
