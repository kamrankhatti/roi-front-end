import { Routes } from '@angular/router';
import { HomeComponent } from './component/home.component';

export const HomeRoutes: Routes = [
    { 'path' : '', component: HomeComponent },
    { 'path' : 'page/:pageNum', component: HomeComponent }
];
