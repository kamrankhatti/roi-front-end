import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap/modal';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {AddPlaylistComponent} from '../modals/add-playlist/add-playlist.component';

// import artist model
import {Artist} from '../../../models/artist';

// import artist service
import {ArtistService} from '../../../services/artist.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    artists:Artist[] = [];
    isBusy = true;
    likedName = [];
    likedArtistName = [];
    modalRef:BsModalRef;

    total = 0;
    page = 1;
    limit = 12;

    /**
     * @param activatedRoute
     * @param artistService
     */
    constructor(private activatedRoute:ActivatedRoute, private artistService:ArtistService, private modalService:BsModalService) {
    }


    ngOnInit() {
        // get page number from query string/parameter
        this.activatedRoute.params.subscribe((params) => {
            this.page = params['pageNum'];
            this.getArtist();
        });
    }

    getArtist() {
        // show loading bar
        this.isBusy = true;
        // set page default value to 1
        this.page = (this.page) ? this.page : 1;
        // get artist name from lcoal storage
        this.likedArtistName = JSON.parse(localStorage.getItem('likedArtistName') || '[]');
        // get artist complete object from local storage
        this.likedName = JSON.parse(localStorage.getItem('likedName') || '[]');

        this.artistService.getArtists(this.page, this.limit).subscribe(data => {
            // get total pages from API response
            this.total = data['topartists']['@attr']['totalPages'];
            // map all artists in artist model
            this.artists = data['topartists']['artist'].map(($item) => {
                return new Artist($item);
            });
            // hide loading bar
            this.isBusy = false;
        }, error => {
            //if found any error redirect to error page
            if(error){
                //router.navigate(['/error'])
            }
        });
    }

    /**
     * check if user already added play list
     * @param row
     * @returns {boolean}
     */
    isAdded(row) {
        return this.likedArtistName.indexOf(row.name) !== -1;
    }

    /**
     *
     * @param row
     */
    addPlaylist(row) {
        const isAdded = this.isAdded(row);

        if (isAdded) {
            this.likedName = this.likedName.filter(($item) => {
                return $item.name !== row.name;
            });

            this.likedArtistName = this.likedArtistName.filter(($item) => {
                return $item !== row.name;
            });
        }

        // set artist title into local storage
        localStorage.setItem('likedArtistName', JSON.stringify(this.likedArtistName));
        // set artist complete object in local storage
        localStorage.setItem('likedName', JSON.stringify(this.likedName));
    }


    goToPage(n:number):void {
        this.page = n;
        this.getArtist();
    }

    onNext():void {
        this.page++;
        this.getArtist();
    }

    onPrev():void {
        this.page--;
        this.getArtist();
    }

    openModal(artist:Artist) {
        this.modalRef = this.modalService.show(AddPlaylistComponent);
        this.modalRef.content.artist = artist;
        this.modalService.onHide.subscribe(() => {
            this.likedArtistName = JSON.parse(localStorage.getItem('likedArtistName') || '[]');
            this.likedName = JSON.parse(localStorage.getItem('likedName') || '[]');
        });
    }

}
