import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { ModalModule } from 'ngx-bootstrap';

import { HomeComponent } from './home.component';
import { PaginationComponent } from '../../shared/components/pagination/pagination.component'
import { ArtistService } from '../../../services/artist.service';
import { Artist } from '../../../models/artist';

describe('HomeComponent', () => {

    let component:HomeComponent;
    let fixture:ComponentFixture<HomeComponent>;
    let de:DebugElement;
    let element:HTMLElement;
    let el;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                HomeComponent,
                PaginationComponent
            ],
            imports: [
                RouterTestingModule,
                HttpClientModule,
                ModalModule.forRoot()
            ],
            providers: [
                ArtistService,
            ]
        })
            .compileComponents();
    }));


    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance; // to access properties and methods

        //mock API response
        const artist =
        {
            "topartists": {
                "artist": [
                    {
                        "name": "David Bowie",
                        "listeners": "3218051",
                        "mbid": "5441c29d-3602-4898-b1a1-b77fa23b8e50",
                        "url": "https://www.last.fm/music/David+Bowie",
                        "streamable": "0",
                        "image": [{
                            "#text": "https://lastfm-img2.akamaized.net/i/u/34s/aa9dfbdec965d751cd1fa4ec5a309299.png",
                            "size": "small"
                        }, {
                            "#text": "https://lastfm-img2.akamaized.net/i/u/64s/aa9dfbdec965d751cd1fa4ec5a309299.png",
                            "size": "medium"
                        }, {
                            "#text": "https://lastfm-img2.akamaized.net/i/u/174s/aa9dfbdec965d751cd1fa4ec5a309299.png",
                            "size": "large"
                        }, {
                            "#text": "https://lastfm-img2.akamaized.net/i/u/300x300/aa9dfbdec965d751cd1fa4ec5a309299.png",
                            "size": "extralarge"
                        }, {
                            "#text": "https://lastfm-img2.akamaized.net/i/u/300x300/aa9dfbdec965d751cd1fa4ec5a309299.png",
                            "size": "mega"
                        }]
                    },]
            }
        };

        component.artists = artist['topartists']['artist'].map(($item) => {
            return new Artist($item);
        });

        de = fixture.debugElement.query(By.css('div'));
        element = de.nativeElement; // to access DOM element

        fixture.detectChanges(); // trigger component change detection
    });


    it('should display Artist name', () => {
        //hide loading image
        component.isBusy = false;
        //detect component change
        fixture.detectChanges();
        //get element inside caption class div
        el = element.querySelectorAll('.caption');
        //compare text inside element
        expect(el[0].children[0].innerText).toContain(component.artists[0]['name']);
    });

    it('should display number of total listeners', () => {
        component.isBusy = false;
        fixture.detectChanges();
        el = element.querySelectorAll('.caption');
        expect(el[0].children[1].innerText).toContain(component.artists[0]['listeners']);
    });

    it('should display Artist thumbnail', () => {
        component.isBusy = false;
        fixture.detectChanges();
        el = element.querySelectorAll('.thumbnail');
        expect(el[0].children[0].src).toContain(component.artists[0]['thumbnail']);
    });

    it("should display 'Remove Playlist' button if playlist is already added", () => {
        component.isBusy = false;
        component.likedArtistName = ['David Bowie'];
        component.isAdded({name: 'David Bowie'});
        fixture.detectChanges();
        el = element.querySelectorAll('.caption');
        const el2 = el[0].children[2].querySelectorAll('a');
        expect(el2[0].innerText).toContain('Remove Playlist');
    });

    it("should display 'Add Playlist' button if playlist is not added", () => {
        component.isBusy = false;
        component.likedArtistName = ['foo'];
        fixture.detectChanges();
        el = element.querySelectorAll('.caption');
        const el2 = el[0].children[2].querySelectorAll('a');
        expect(el2[0].innerText).toContain('Add Playlist');
    });

    it("should display 'View Details' button", () => {
        component.isBusy = false;
        fixture.detectChanges();
        el = element.querySelectorAll('.caption');
        const el2 = el[0].children[2].querySelectorAll('a');
        expect(el2[1].innerText).toContain('View Details');
    });

});

