import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

// import artist model
import { ArtistDetail } from '../../../../models/artist.detail';

// import service
import {ArtistService} from '../../../../services/artist.service';

@Component({
    selector: 'app-artist-detail',
    templateUrl: './artist-detail.component.html',
    styleUrls: ['./artist-detail.component.css']
})

export class ArtistDetailComponent implements OnInit {

    artistName:string;
    tag:string;
    isBusy = true;
    artists = {};

    /**
     * @param activatedRoute
     * @param artistService
     */
    constructor(private activatedRoute:ActivatedRoute, private artistService:ArtistService) {
    }


    ngOnInit() {
        // get title from parameter/query string
        this.activatedRoute.params.subscribe((params) => {
            this.artistName = params['title'];
            this.getDetails();
        });
    }

    /**
     * get complete details of particular artist by passing his/her name
     * in service method.
     */
    getDetails() {
        const artistName = encodeURIComponent(this.artistName);
        this.artistService.getArtistDetails(artistName).subscribe(data => {
            this.isBusy = false;
            this.artists = this.getArtistDetail(data);
        }, error => {
            //if found any error redirect to error page
            if (error) {
                //router.navigate(['/error'])
            }
        });
    }


    /**
     * Get and load details of artist in a model.
     * @param data
     * @returns {ArtistDetail}
     */
    getArtistDetail(data) {
        return new ArtistDetail(data);
    }
}
