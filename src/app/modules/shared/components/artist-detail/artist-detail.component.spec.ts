import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared.module';
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";

import { ArtistDetailComponent } from './artist-detail.component';
import { ArtistService } from '../../../../services/artist.service';


describe('ArtistDetailComponent', () => {
    let component:ArtistDetailComponent;
    let fixture:ComponentFixture<ArtistDetailComponent>;
    let de:DebugElement;
    let element:HTMLElement;
    let el;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ArtistDetailComponent
            ],
            imports: [
                RouterTestingModule,
                SharedModule
            ],
            providers: [
                ArtistService,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArtistDetailComponent);
        component = fixture.componentInstance;
        component.isBusy = false;
        component.artists =
        {
            thumbnail: 'dumyimage.jpg',
            name: 'David Bowie',
            listeners: 78542239,
            playcount: 85620045,
            bio: 'David is a great singer...',

        };
        de = fixture.debugElement.query(By.css('div'));
        element = de.nativeElement;

        fixture.detectChanges();
    });

    it('should display Artist name', () => {
        el = element.querySelectorAll('.caption');
        expect(el[0].children[0].innerText).toContain(component.artists['name']);
    });

    it('should display Artist image', () => {
        el = element.querySelectorAll('.thumbnail');
        expect(el[0].children[0].src).toContain(component.artists['thumbnail']);
    });

    it('should display number of listeners', () => {
        el = element.querySelectorAll('.caption');
        expect(el[0].children[1].innerText).toContain(component.artists['listeners']);
    });

    it('should display number of playcount', () => {
        el = element.querySelectorAll('.caption');
        expect(el[0].children[2].innerText).toContain(component.artists['playcount']);
    });

    it('should display complete bio', () => {
        expect(el[0].children[3].innerText).toContain(component.artists['bio']);
    });

});
