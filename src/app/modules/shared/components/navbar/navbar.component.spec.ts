import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";
import { CollapseModule } from 'ngx-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';

import { NavbarComponent } from './navbar.component';


describe('NavbarComponent', () => {
    let component:NavbarComponent;
    let fixture:ComponentFixture<NavbarComponent>;
    let de:DebugElement;
    let element:HTMLElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NavbarComponent],
            imports: [
                CollapseModule,
                RouterTestingModule,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NavbarComponent);
        component = fixture.componentInstance;

        de = fixture.debugElement.query(By.css('nav div ul'));
        element = de.nativeElement;
        fixture.detectChanges();
    });

    it("should display 'Home' menu", () => {
        expect(element.innerHTML.indexOf('Home') !== -1).toBe(true);
    });

    it("should display 'Playlists' menu", () => {
        expect(element.innerHTML.indexOf('Playlists') !== -1).toBe(true);
    });


});
