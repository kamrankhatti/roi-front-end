import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CollapseModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule }   from '@angular/forms';
import { RouterModule} from '@angular/router';

/* Component */
import { PaginationComponent } from './components/pagination/pagination.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CollapseModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    CollapseModule,
    FormsModule,
    ModalModule,
    PaginationComponent
  ],
  providers: [HttpClient],
  declarations: [PaginationComponent]
})
export class SharedModule { }
