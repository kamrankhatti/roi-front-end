import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";

import { PlaylistComponent } from './playlist.component';


describe('PlaylistComponent', () => {

    let component:PlaylistComponent;
    let fixture:ComponentFixture<PlaylistComponent>;
    let de:DebugElement;
    let element:HTMLElement;
    let el;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                PlaylistComponent,
            ],
            imports: [RouterTestingModule]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlaylistComponent);
        component = fixture.componentInstance;

        const artist =
        {
            "topartists": {
                "artist": [
                    {
                        "name": "David Bowie",
                        "listeners": "3218051",
                        "mbid": "5441c29d-3602-4898-b1a1-b77fa23b8e50",
                        "url": "https://www.last.fm/music/David+Bowie",
                        "streamable": "0",
                        "thumbnail": 'test.jpg',
                        "tag": 'some tag',
                    },]
            }
        };

        //set mocked data into local storage
        localStorage.setItem('likedArtistName', JSON.stringify(['David Bowie']));
        localStorage.setItem('likedName', JSON.stringify(artist['topartists']['artist']));

        //get mocked data from local storage
        component.likedArtistName = JSON.parse(localStorage.getItem('likedArtistName') || '[]');
        component.likedName = JSON.parse(localStorage.getItem('likedName') || '[]');
        component.artists = JSON.parse(localStorage.getItem('likedName') || '[]');

        de = fixture.debugElement.query(By.css('div'));
        element = de.nativeElement;

        fixture.detectChanges();
    });

    it('should display Artist name from localStorage', () => {
        el = element.querySelectorAll('.caption');
        expect(el[0].children[0].innerText).toContain(component.artists[0]['name']);
    });

    it('should display Artist image from localStorage', () => {
        el = element.querySelectorAll('.thumbnail');
        expect(el[0].children[0].src).toContain(component.artists[0]['thumbnail']);
    });

    it('should display Artist image from localStorage', () => {
        el = element.querySelectorAll('.caption');
        expect(el[0].children[1].innerText).toContain(component.artists[0]['listeners']);
    });

    it('should display Tag from localStorage', () => {
        el = element.querySelectorAll('.caption');
        expect(el[0].children[2].innerText).toContain(component.artists[0]['tag']);
    });

    it("should display 'Remove Playlist' button if playlist is availabe in localStorage", () => {
        component.likedArtistName = ['David Bowie'];
        component.isAdded({name: 'David Bowie'});
        fixture.detectChanges();
        el = element.querySelectorAll('.caption');
        const el2 = el[0].children[3].querySelectorAll('a');
        expect(el2[0].innerText).toContain('Remove Playlist');
    });

    it("should display 'Add Playlist' button if playlist is not available in localStorage", () => {
        component.likedArtistName = ['foo'];
        fixture.detectChanges();
        el = element.querySelectorAll('.caption');
        const el2 = el[0].children[3].querySelectorAll('a');
        expect(el2[0].innerText).toContain('Add Playlist');
    });

    it("should display 'View Details' button", () => {
        fixture.detectChanges();
        el = element.querySelectorAll('.caption');
        const el2 = el[0].children[3].querySelectorAll('a');
        expect(el2[1].innerText).toContain('View Details');
    });

});
