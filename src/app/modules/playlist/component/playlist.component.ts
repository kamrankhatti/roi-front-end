import { Component, OnInit } from '@angular/core';
import { Artist } from '../../../models/artist';

@Component({
    selector: 'app-playlist',
    templateUrl: './playlist.component.html',
    styleUrls: ['./playlist.component.css']
})

export class PlaylistComponent implements OnInit {

    artists: Artist[] = [];
    likedName = [];
    likedArtistName = [];

    constructor() {
    }

    ngOnInit() {
        this.likedArtistName = JSON.parse(localStorage.getItem('likedArtistName') || '[]');
        this.likedName = JSON.parse(localStorage.getItem('likedName') || '[]');
        this.artists = JSON.parse(localStorage.getItem('likedName') || '[]');
    }


    isAdded(row) {
        return this.likedArtistName.indexOf(row.name) !== -1;
    }

    addPlaylist(row) {
        const isAdded = this.isAdded(row);

        if (isAdded) {
            this.likedName = this.likedName.filter(($item) => {
                return $item.name !== row.name;
            });

            this.likedArtistName = this.likedArtistName.filter(($item) => {
                return $item !== row.name;
            });

            this.artists = this.artists.filter(($item) => {
                return $item.name !== row.name;
            });
        }

        localStorage.setItem('likedArtistName', JSON.stringify(this.likedArtistName));
        localStorage.setItem('likedName', JSON.stringify(this.likedName));
    }
}
