import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PlaylistRoute } from './playlist.routes';
import { PlaylistComponent } from './component/playlist.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(PlaylistRoute)
  ],
  declarations: [
    PlaylistComponent
  ]
})
export class PlaylistModule { }
