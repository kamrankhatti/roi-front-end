import { Routes } from '@angular/router';
import { PlaylistComponent  } from './component/playlist.component';

export const PlaylistRoute: Routes = [
    { 'path' : '', component: PlaylistComponent }
];
