export class Artist {
    name: string;
    thumbnail: {};
    listeners: string;

    constructor(row: any) {
        this.name = row.name || '';
        this.thumbnail = row.image[3]['#text'] || { thumbnail: 'http://cohenwoodworking.com/wp-content/uploads/2016/09/image-placeholder-500x500.jpg' };
        this.listeners = row.listeners || '';
    }
}
