export class ArtistDetail {
    name: string;
    thumbnail: {};
    listeners: string;
    playcount: string;
    bio: string;

    constructor(data: any) {
        this.name = data['artist']['name'] || '';
        this.listeners = data['artist']['stats']['listeners'] || '';
        this.playcount = data['artist']['stats']['playcount'] || '';
        this.bio = data['artist']['bio']['content'] || '';
        this.thumbnail = data['artist']['image'][5]['#text'] || {thumbnail: 'http://cohenwoodworking.com/wp-content/uploads/2016/09/image-placeholder-500x500.jpg'};
    }
}
