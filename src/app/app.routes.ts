import { Routes } from '@angular/router';
import { ArtistDetailComponent } from './modules/shared/components/artist-detail/artist-detail.component';

export const AppRoutes: Routes = [
    { 'path' : 'artist/:title', component: ArtistDetailComponent },
    { 'path' : 'playlists', loadChildren: './modules/playlist/playlist.module#PlaylistModule' },
    { 'path' : '', loadChildren: './modules/home/home.module#HomeModule' }
];