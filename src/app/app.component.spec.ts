import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('AppComponent', () => {
    const component = new AppComponent();

    it('should create the app', async(() => {
        expect(component).toBeTruthy();
    }));

    it(`should have a title 'app'`, async(() => {
        expect(component.title).toEqual('app');
    }));
});
