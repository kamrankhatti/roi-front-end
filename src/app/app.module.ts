import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { SharedModule } from './modules/shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';

/*Routes*/
import { AppRoutes } from './app.routes';

/*Components*/
import { AppComponent } from './app.component';
import { ArtistDetailComponent } from './modules/shared/components/artist-detail/artist-detail.component';
import { NavbarComponent } from './modules/shared/components/navbar/navbar.component';
import { ArtistService } from './services/artist.service';


@NgModule({
  declarations: [
    AppComponent,
    ArtistDetailComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
  ],
  providers: [ArtistService],
  bootstrap: [AppComponent],
})
export class AppModule { }
