import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

//
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ArtistService {

    constructor(private http: HttpClient) {}

    /**
     * Get list of all artis
     * @param page
     * @param limit
     * @returns {Observable<Object>}
     */
    getArtists(page: number, limit: number): Observable<any> {
        const apiURL = `${environment.LastFMApiBaseURL}?method=geo.gettopartists&country=spain&api_key=${environment.ApiKey}&format=json&page=${page}&limit=${limit}`;
        return this.http.get(apiURL);
    }


    /**
     * Get detail of any particular artist
     * @param artistName
     * @returns {Observable<Object>}
     */
    getArtistDetails(artistName: string): Observable<any>{
        const apiURL = `${environment.LastFMApiBaseURL}?method=artist.getinfo&artist=${artistName}&api_key=${environment.ApiKey}&format=json`;
        return this.http.get(apiURL);
    }

}
