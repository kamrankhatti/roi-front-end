import { TestBed, async, inject } from '@angular/core/testing';
import { BaseRequestOptions, Response, ResponseOptions, Http, HttpModule, XHRBackend } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { ArtistService } from './artist.service';

describe('ArtistService', () => {
  let mockbackend, service;

  // setup
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule, HttpClientModule],
      providers: [
        ArtistService,
        { provide: XHRBackend, useClass: MockBackend }
      ]
    });
  });

  beforeEach(inject([ArtistService, XHRBackend], (_service, _mockbackend) => {
    service = _service;
    mockbackend = _mockbackend;
  }));
  // specs
  it('should return mocked(API service response) all Artists', () => {/*  */
    const response = {
      "topartists": {
        "artist": [
          {
            "name": "David Bowie",
            "listeners": "3218051",
            "mbid": "5441c29d-3602-4898-b1a1-b77fa23b8e50",
            "url": "https://www.last.fm/music/David+Bowie",
            "streamable": "0",
            "image": [
              {
                "#text": "https://lastfm-img2.akamaized.net/i/u/34s/aa9dfbdec965d751cd1fa4ec5a309299.png",
                "size": "small"
              }
            ]
          },
          {
            "name": "Radiohead",
            "listeners": "4604703",
            "mbid": "a74b1b7f-71a5-4011-9441-d0b5e4122711",
            "url": "https://www.last.fm/music/Radiohead",
            "streamable": "0",
            "image": [
              {
                "#text": "https://lastfm-img2.akamaized.net/i/u/34s/d15616ecf0033a2490899e1045489515.png",
                "size": "small"
              }
            ]
          }
        ],
        "@attr": {
          "country": "Spain",
          "page": "1",
          "perPage": "2",
          "totalPages": "270021",
          "total": "540042"
        }
      }
    };
    mockbackend.connections.subscribe(connection => {
      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(response)
      })));
    });
    service.getArtists(1, 2).subscribe(artists => {
      const mockArtist = {
        "name": "David Bowie",
        "listeners": "3218051",
        "mbid": "5441c29d-3602-4898-b1a1-b77fa23b8e50",
        "url": "https://www.last.fm/music/David+Bowie",
        "streamable": "0",
        "image": [
          {
            "#text": "https://lastfm-img2.akamaized.net/i/u/34s/aa9dfbdec965d751cd1fa4ec5a309299.png",
            "size": "small"
          }
        ]
      };
      expect(artists.topartists.artists[0]).toBe(mockArtist);
      expect(artists.topartists.artist.length).toBe(2);
    });
  });

  it('should return mocked(API service response) of specific Artist details', () => {
    const response = {
      "artist": {
        "name": "Queen",
        "mbid": "420ca290-76c5-41af-999e-564d7c71f1a7",
        "url": "https://www.last.fm/music/Queen",
        "image": [{
          "#text": "https://lastfm-img2.akamaized.net/i/u/34s/b7ae66009a3845528a51cd06f5d5ae22.png",
          "size": "small"
        }, {
          "#text": "https://lastfm-img2.akamaized.net/i/u/64s/b7ae66009a3845528a51cd06f5d5ae22.png",
          "size": "medium"
        }, {
          "#text": "https://lastfm-img2.akamaized.net/i/u/174s/b7ae66009a3845528a51cd06f5d5ae22.png",
          "size": "large"
        }, {
          "#text": "https://lastfm-img2.akamaized.net/i/u/300x300/b7ae66009a3845528a51cd06f5d5ae22.png",
          "size": "extralarge"
        }, {
          "#text": "https://lastfm-img2.akamaized.net/i/u/300x300/b7ae66009a3845528a51cd06f5d5ae22.png",
          "size": "mega"
        }, {
          "#text": "https://lastfm-img2.akamaized.net/i/u/300x300/b7ae66009a3845528a51cd06f5d5ae22.png",
          "size": ""
        }],
        "streamable": "0",
        "ontour": "1",
        "stats": { "listeners": "3815610", "playcount": "169251728" },

        "bio": {
          "links": { "link": { "#text": "", "rel": "original", "href": "https://last.fm/music/Queen/+wiki" } },
          "published": "10 Feb 2006, 19:01",
          "summary": "Queen were an English rock band originally...",
          "content": "Queen were an English rock band originally..."
        }
      }
    };
    mockbackend.connections.subscribe(connection => {
      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(response)
      })));
    });
    service.getArtistDetails('Queen').subscribe(artists => {
      const mockArtist = {
        "artist": {
          "name": "Queen",
          "mbid": "420ca290-76c5-41af-999e-564d7c71f1a7",
          "url": "https://www.last.fm/music/Queen",
          "image": [{
            "#text": "https://lastfm-img2.akamaized.net/i/u/34s/b7ae66009a3845528a51cd06f5d5ae22.png",
            "size": "small"
          }, {
            "#text": "https://lastfm-img2.akamaized.net/i/u/64s/b7ae66009a3845528a51cd06f5d5ae22.png",
            "size": "medium"
          }, {
            "#text": "https://lastfm-img2.akamaized.net/i/u/174s/b7ae66009a3845528a51cd06f5d5ae22.png",
            "size": "large"
          }, {
            "#text": "https://lastfm-img2.akamaized.net/i/u/300x300/b7ae66009a3845528a51cd06f5d5ae22.png",
            "size": "extralarge"
          }, {
            "#text": "https://lastfm-img2.akamaized.net/i/u/300x300/b7ae66009a3845528a51cd06f5d5ae22.png",
            "size": "mega"
          }, {
            "#text": "https://lastfm-img2.akamaized.net/i/u/300x300/b7ae66009a3845528a51cd06f5d5ae22.png",
            "size": ""
          }],
          "streamable": "0",
          "ontour": "1",
          "stats": { "listeners": "3815610", "playcount": "169251728" },

          "bio": {
            "links": {
              "link": {
                "#text": "",
                "rel": "original",
                "href": "https://last.fm/music/Queen/+wiki"
              }
            },
            "published": "10 Feb 2006, 19:01",
            "summary": "Queen were an English rock band originally...",
            "content": "Queen were an English rock band originally..."
          }
        }
      };
      expect(artists.artist).toBe(mockArtist);
    });
  });

});
